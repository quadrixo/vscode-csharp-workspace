# Change Log

All notable changes to the "csharp-workspace" extension will be documented in this file.

Check [Keep a Changelog](http://keepachangelog.com/) for recommendations on how to structure this file.

## [0.9.2] - 2021-03-16
### Fixed

- Missing await keyword on solution file opening

## [0.9.1] - 2021-03-16
### Changed

- Change from qp to quadrixo publisher

## [0.9.0] - 2021-03-12
### Changed

- Handle nested projects from solution file

## [0.1.3] - 2019-06-19
### Added

- Add API controller command
- Add Class with namespace resolution
- Open Solution file (.sln) as a VSCode workspace
