# vscode-csharp-workspace
> Useful commands for C# development.

This extension adds useful commands and menu entries for C# development.

## Installing

```shell
ext install qp.csharp-workspace
```

## Usage

Hit `Ctrl+Shift+P` and type `CSharp` to select command.

## Features

 * __Add Class...__: Create a class file, or an unamed document with namespace resolution
 * __Add API Controller...__: Create a controller file, or an unamed document with namespace resolution
 * __Open Solution...__: Open projects' folder from a solution file

> __Open Solution...__ replace current workspace

## Public API

Commands are accessible with the public API.

```typescript
let csharpUtils = vscode.extensions.getExtension('qp.csharp-workspace').exports;
```

### Add class

```typescript
addClass(classname: string, namespace?: string, directory?: vscode.Uri): Promise<void>
```

Create a new class definition with the name `classname` in the namespace
`namespace` into the destination folder `directory`.

 * __classname__ (string, mandatory): the name of the class
 * __namespace__ (string, optional): the namespace
 * __directory__ (vscode.Uri: optional): the destination folder

If `namespace` is undefined, it will be resolved against the directory, otherwise *Default* is used.
If `directory` is undefined, no file is created, but an unamed file is open.

### Add API controller

```typescript
addApiController(controllername: string, namespace?: string, directory?: vscode.Uri): Promise<void>
```

Create a new API controller definition with the name `controllername` in the namespace
`namespace` into the destination folder `directory`.

 * __controllername__ (string, mandatory): the name of the controller (if the name doesn't ends with _Controller_,
                                           it is automatically appended)
 * __namespace__ (string, optional): the namespace
 * __directory__ (vscode.Uri: optional): the destination folder

If `namespace` is undefined, it will be resolved against the directory, otherwise *Default* is used.
If `directory` is undefined, no file is created, but an unamed file is open.

### Open Solution

```typescript
openSln(sln: vscode.Uri): Promise<void>
```

Replace current workspace with projects listed into the given `.sln` file.

 * __sln__ (vscode.Uri, mandatory): the uri of the `.sln` file

### Resolve namespace

```typescript
resolveNamespace(path: vscode.Uri): Promise<string>
```

Provides the mechanism to resolve namespace.

 * __path__ (vscode.Uri, mandatory): a full path to the destination folder.

## Licensing

The code in this project is licensed under CeCILL license. See LICENSE.txt file for more information.
