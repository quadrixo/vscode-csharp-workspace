import * as nodefs from 'fs';
import * as path from 'path';
import * as vscode from 'vscode';

const DEFAULT_NAMESPACE = "Default";
const SEP_SEARCH = path.sep === '\\' ? /\\/g : /\//g;

export class ProjectContext {

    readonly rootnamespace: string;
    readonly rootpath: vscode.Uri;

    // Constructor
    constructor(readonly csproj: vscode.Uri) {
        let content = nodefs.readFileSync(csproj.fsPath);
        let matches = content.toString().match(/<RootNamespace>([\w.]+)<\/RootNamespace>/);
        this.rootnamespace = matches ? matches[1] : path.parse(csproj.fsPath).name;
        this.rootpath = vscode.Uri.file(path.dirname(csproj.fsPath));
    }

    public contains(uri: vscode.Uri): boolean {
        return uri.fsPath === this.rootpath.fsPath || uri.fsPath.startsWith(`${this.rootpath.fsPath}${path.sep}`);
    }

    public resolve(directory: vscode.Uri): string {
        if (!this.contains(directory)) {
            return DEFAULT_NAMESPACE;
        }

        let relative = path.relative(this.rootpath.fsPath, directory.fsPath)
            .replace(/(^|[.\/\\])([0-9])/g, "$1_$2")
            .replace(SEP_SEARCH, '.')
            .replace(/^\.+|\.+$/g, '');

        return relative ? `${this.rootnamespace}.${relative}` : this.rootnamespace;
    }
}
