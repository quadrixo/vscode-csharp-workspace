import * as path from "path";
import * as vscode from "vscode";
import { ExtensionContext } from "./extension-context";

const fs = vscode.workspace.fs;

export const TPL_CLASS = "Class.tpl";
export const TPL_API_CONTROLLER = "ApiController.tpl";

export class TemplateProcessor {
    // Constructor
    constructor(readonly context: ExtensionContext) {
    }

    public async process(template: string, destination?: vscode.Uri, data?: { [key: string]: string }): Promise<vscode.TextDocument> {
        const source = vscode.Uri.file(path.join(this.context.extensionPath, 'templates', template));

        let declaration = Buffer.from(await fs.readFile(source)).toString();
        for (let property in data) {
            declaration = declaration.replace(`\$${property}\$`, data[property]);
        }

        if (destination) {
            await fs.writeFile(destination, Buffer.from(declaration));
            return await vscode.workspace.openTextDocument(destination);
        }

        return await vscode.workspace.openTextDocument({ language: 'csharp', content: declaration });
    }
}
