import * as fs from 'fs';

const callback = (resolve: Function, reject: Function) =>
    (error: NodeJS.ErrnoException|null, result?: any) => {
        if (error) {
            reject(error);
        }
        else {
            resolve(result);
        }
    };

export function stat(path: fs.PathLike): Promise<fs.Stats> {
    return new Promise<fs.Stats>((resolve, reject) => fs.stat(path, callback(resolve, reject)));
}
export function readdir(path: fs.PathLike, options?: { encoding?: string | null; withFileTypes?: false } | string): Promise<string[]> {
    return new Promise<string[]>((resolve, reject) => fs.readdir(path, options, callback(resolve, reject)));
}
export function readFile(path: fs.PathLike | number, options?: { encoding?: string; flag?: string; } | string): Promise<string | Buffer> {
    return new Promise<string | Buffer>((resolve, reject) => fs.readFile(path, options, callback(resolve, reject)));
}
export function writeFile(path: fs.PathLike | number, data: any, options?: fs.WriteFileOptions): Promise<void> {
    return new Promise<void>((resolve, reject) => fs.writeFile(path, data, options || 'utf8', callback(resolve, reject)));
}
export function mkdir(path: fs.PathLike, options: number | string | fs.MakeDirectoryOptions | undefined | null): Promise<fs.PathLike> {
    return new Promise<fs.PathLike>((resolve, reject) => {
        try {
            fs.accessSync(path);
            return resolve(path);
        }
        catch {
            fs.mkdir(path, options, callback(() => resolve(path), reject));
        }
    });
}
