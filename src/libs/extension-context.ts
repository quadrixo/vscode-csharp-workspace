import * as vscode from 'vscode';
import { ProjectContext } from './project-context';
import { promises as fs } from 'fs';
import * as path from 'path';

export const DEFAULT_NS = 'Default';

export type ExtensionContext = ExtensionContextImpl & vscode.ExtensionContext;

type Path = { fullpath: vscode.Uri, directory: vscode.Uri };

async function pathify(uri: vscode.Uri): Promise<Path> {
    let stat = await fs.stat(uri.fsPath);
    let directory = stat.isFile() ? path.dirname(uri.fsPath) : uri.fsPath;

    return { fullpath: uri, directory: vscode.Uri.file(directory) };
}

/**
 * Returns an augmented context for this extention
 *
 * @param context The current `vscode.ExtensionContext`
 */
export async function factory(vscontext: vscode.ExtensionContext): Promise<ExtensionContext> {
    let context = new ExtensionContextImpl(vscontext);
    await context.update();
    return <ExtensionContext>context;
}

/**
 * Execution context for the extension
 */
class ExtensionContextImpl {

    readonly projects: ProjectContext[] = [];

    // Constructor
    constructor(private readonly context: vscode.ExtensionContext) {
        vscode.workspace.onDidChangeWorkspaceFolders(_ => this.update());
    }

    // Wrapper for `vscode.ExtensionContext.asAbsolutePath()`
    asAbsolutePath(relativePath: string): string {
        return this.context.asAbsolutePath(relativePath);
    }

    /**
     * Returns the context of the project which contains the given uri
     *
     * @param uri An URI
     */
    public async getProjectContext(uri: vscode.Uri): Promise<ProjectContext | undefined> {
        return this.project(await pathify(uri));
    }

    /**
     * Resolve the namespace for a class created in the given directory.
     *
     * If no path given, the default namespace is returned.
     *
     * @param uri An URI
     */
    public async resolveNamespace(uri?: vscode.Uri): Promise<string> {
        if (!uri) {
            return DEFAULT_NS;
        }

        await fs.mkdir(uri.path, { mode: 0o755, recursive: true });
        let pathified = await pathify(uri);
        let project = this.project(pathified);
        if (project === undefined) {
            return DEFAULT_NS;
        }

        let relative = path.relative(project.rootpath.fsPath, pathified.directory.fsPath)
            .replace(/^[./\\]+|[./\\]+$/g, "")
            .replace(/\.([^a-zA-Z_])/g, "._$1");
        relative = relative ? `.${relative}` : "";
        return `${project.rootnamespace}${relative}`;
    }

    // Returns the context of the project which contains the given path
    private project(fullpath: Path): ProjectContext | undefined {
        let project: ProjectContext|undefined = undefined;
        for (let p of this.projects) {
            if (p.contains(fullpath.directory)) {
                if (project === undefined || project.rootpath.fsPath.length < p.rootpath.fsPath.length) {
                    project = p;
                }
            }
        }

        return project;
    }

    public async update(): Promise<void> {
        let files = await vscode.workspace.findFiles("**/*.csproj");
        this.projects.splice(0, this.projects.length, ...files.map(f => new ProjectContext(f)));
    }
}

// Wrapper for vscode.ExtensionContext.* properties
Object.defineProperty(ExtensionContextImpl.prototype, "subscriptions", { get: function() { return this.context.subscriptions; }, enumerable: true, configurable: true });
Object.defineProperty(ExtensionContextImpl.prototype, "workspaceState", { get: function() { return this.context.workspaceState; }, enumerable: true, configurable: true });
Object.defineProperty(ExtensionContextImpl.prototype, "globalState", { get: function() { return this.context.globalState; }, enumerable: true, configurable: true });
Object.defineProperty(ExtensionContextImpl.prototype, "extensionPath", { get: function() { return this.context.extensionPath; }, enumerable: true, configurable: true });
Object.defineProperty(ExtensionContextImpl.prototype, "storagePath", { get: function() { return this.context.storagePath; }, enumerable: true, configurable: true });
Object.defineProperty(ExtensionContextImpl.prototype, "globalStoragePath", { get: function() { return this.context.globalStoragePath; }, enumerable: true, configurable: true });
Object.defineProperty(ExtensionContextImpl.prototype, "logPath", { get: function() { return this.context.logPath; }, enumerable: true, configurable: true });
