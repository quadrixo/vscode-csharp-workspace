//
// Note: This example test is leveraging the Mocha test framework.
// Please refer to their documentation on https://mochajs.org/ for help.
//

// The module "assert" provides assertion methods from node
import * as assert from "assert";
import * as path from "path";
import * as process from "process";
import * as vscode from "vscode";

// You can import and use all API from the "vscode" module
// as well as import your extension to test it
// import * as vscode from "vscode";
// import * as myExtension from "../extension";
import { ProjectContext } from "../../libs/project-context";

const ASSETS = path.join(process.cwd(), 'src', 'test', 'assets');

function resolve(context: ProjectContext, dir: string, expected: string): void {
    let ns = context.resolve(vscode.Uri.file(dir));
    assert.strictEqual(ns, expected);
}

// Defines a Mocha test suite to group tests of similar kind together
suite("Extension Tests", function () {
    // Defines a Mocha unit test
    test("ProjectContext.resolve()", () => {
        let csproj = vscode.Uri.file(path.join(ASSETS, "test1", "QuadriPlus.Test.csproj"));
        let context = new ProjectContext(csproj);

        resolve(context, "/some", "Default");
        resolve(context, path.join(ASSETS, "test1"), "QuadriPlus.Test");
        resolve(context, path.join(ASSETS, "test1", "Some", "Place"), "QuadriPlus.Test.Some.Place");
        // ProjectContext.resolve() is not ment to be public, the argument is handle as a directory.
        resolve(context, path.join(ASSETS, "test1", "QuadriPlus.Test.csproj"), "QuadriPlus.Test.QuadriPlus.Test.csproj");
    });

    let contains = (context: ProjectContext, dir: string, expected: boolean) =>
        assert.strictEqual(context.contains(vscode.Uri.file(dir)), expected);

    test("ProjectContext.contains()", () => {
        let csproj = vscode.Uri.file(path.join(ASSETS, "test1", "QuadriPlus.Test.csproj"));
        let context = new ProjectContext(csproj);

        contains(context, path.join(ASSETS, "test1"), true);
        contains(context, path.join(ASSETS, "test1", "QuadriPlus.Test.csproj"), true);
        contains(context, path.join(ASSETS, "test1", "Some", "Place"), true);
        contains(context, path.join(ASSETS, "test1") + path.sep, true);
        contains(context, path.join(ASSETS, "test1", "Some", "Place") + path.sep, true);
        contains(context, "/var/run", false);
        contains(context, path.resolve("/some/path"), false);
    });
});
