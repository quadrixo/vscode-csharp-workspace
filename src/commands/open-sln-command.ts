import * as vscode from 'vscode';
import * as path from 'path';

import { ICommand } from './command';

const OPEN_SLN_COMMAND_ID = 'csharp.openSln';

/**
 * Argument definition for [[OpenSlnCommand.handle]].
 */
export interface IOpenSlnCommand {
    sln: vscode.Uri;
}

/**
 * Add projects from the selected `.sln` into the current workspace.
 */
export class OpenSlnCommand implements ICommand<IOpenSlnCommand, void> {
    public readonly id: string = OPEN_SLN_COMMAND_ID;

    // Constructor
    constructor(readonly context: vscode.ExtensionContext) {
    }

    /**
     * Add a class
     *
     * @param command arguments
     */
    public async handle(command: IOpenSlnCommand): Promise<void> {
        if (command.sln) {
            const buffer = Buffer.from(await vscode.workspace.fs.readFile(command.sln));
            const root = path.dirname(command.sln.fsPath);

            const projects: { key: string; name: string; uri: vscode.Uri }[] = [];
            const dirs: {[key: string]: string} = {};
            const nestedProjects = new Map<string, string>();
            let parserState = 0;
            let parsed = '';
            for (const line of OpenSlnCommand.lines(buffer)) {
                if (line.match('GlobalSection\\(NestedProjects\\)')) {
                    parserState = 2;
                }
                else if (line.match('Global')) {
                    parserState = 1;
                }

                if (parserState === 0) {
                    if (line.match('EndProject')) {
                        let [_, name, filepath, key] = parsed.split(/[=,]/).map(p => p.replace(/^[ "{}]+/, '').replace(/[ "{}]+$/, ''));
                        if (filepath.endsWith('.csproj')) {
                            const uri = path.dirname(path.resolve(root, filepath.replace(/\\/g, path.sep)));
                            projects.push({key: key, name: name, uri: vscode.Uri.file(uri)});
                        }
                        else {
                            dirs[key] = name;
                        }
                    }
                    else if (line.match(/Project\(/)) {
                        parsed = line.trim();
                    }
                    else {
                        parsed += line.trim();
                    }
                }
                else if (parserState === 2) {
                    const matches = line.match(/\{([0-9A-Fa-f-]+)\}\s*=\s*\{([0-9A-Fa-f-]+)\}/);
                    if (matches) {
                        nestedProjects.set(matches[1], matches[2]);
                    }
                }
            }

            for (const project of projects) {
                let parent = nestedProjects.get(project.key);
                while (parent) {
                    project.name = dirs[parent] + '/' + project.name;
                    parent = nestedProjects.get(parent);
                }
            }

            vscode.workspace.updateWorkspaceFolders(
                0,
                vscode.workspace.workspaceFolders?.length,
                ...projects.sort((p1, p2) => p1.name.localeCompare(p2.name))
            );
        }
    }

    public async convert(): Promise<IOpenSlnCommand> {
        var sln = await vscode.window.showOpenDialog({
            canSelectMany: false,
            // eslint-disable-next-line @typescript-eslint/naming-convention
            filters: { 'Solution': ['sln'] }
        });

        if (sln && sln.length > 0) {
            return { sln: sln[0] };
        }

        throw new Error("No file selected");
    }

    private static *lines(buffer: Buffer): Generator<string, string, void> {
        const linebreak = ["\r".charCodeAt(0), "\n".charCodeAt(0)];
        let line = '';
        let isEol = false;
        for (const c of buffer) {
            if (linebreak.includes(c)) {
                isEol = true;
            }
            else if (isEol) {
                yield line;
                line = String.fromCharCode(c);
                isEol = false;
            }
            else {
                line += String.fromCharCode(c);
            }
        }
        return line;
    }
}
