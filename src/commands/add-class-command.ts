import * as vscode from 'vscode';
import * as nls from 'vscode-nls';
import * as path from 'path';

import { ICommand } from './command';
import { ExtensionContext } from '../libs/extension-context';
import * as tpl from '../libs/template-processor';

const COMMAND_ID = 'csharp.addClass';
const localize = nls.config({ messageFormat: nls.MessageFormat.file })();

/**
 * Argument definition for [[AddClassCommand.handle]].
 */
export interface IAddClassCommand {
    /** The path to the directory where to create the class file */
    directory?: vscode.Uri;

    /** The namepace */
    namespace?: string;

    /** The name of the class */
    classname: string;
}

/**
 * Create a new class file and open it.
 */
export class AddClassCommand implements ICommand<IAddClassCommand, void> {
    public readonly id: string = COMMAND_ID;

    // Constructor
    constructor(readonly context: ExtensionContext) {
    }

    /**
     * Add a class
     *
     * @param command arguments
     */
    public async handle(command: IAddClassCommand): Promise<void> {
        if (!command.classname || !/^[\p{L}_][\p{L}0-9_]*$/u.test(command.classname)) {
            throw new Error(localize('warning.noClassName', "Invalid class name"));
        }

        if (!command.namespace) {
            command.namespace = await this.context.resolveNamespace(command.directory);
        }

        const destination = command.directory ? vscode.Uri.joinPath(command.directory, `${command.classname}.cs`) : undefined;
        const data = {
            rootnamespace: command.namespace,
            safeitemname: command.classname
        };

        new tpl.TemplateProcessor(this.context)
            .process(tpl.TPL_CLASS, destination, data)
            .then(document => vscode.window.showTextDocument(document, { selection: new vscode.Range(9, 8, 9, 8) }));
    }

    public async convert(context: vscode.Uri): Promise<IAddClassCommand|undefined> {
        let directory = context;
        if (context) {
            const stat = await vscode.workspace.fs.stat(context);
            if (stat.type === vscode.FileType.File) {
                directory = vscode.Uri.file(path.dirname(context.fsPath));
            }
        }

        let prompt = localize('prompt.Classname', "The name of the class");
        let classname = await vscode.window.showInputBox({
            placeHolder: 'Classname',
            prompt: prompt
        });

        if (classname === undefined || classname === null) {
            return undefined;
        }

        return { directory: directory, classname: classname };
    }
}
