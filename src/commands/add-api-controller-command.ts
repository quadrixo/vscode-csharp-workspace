import * as vscode from 'vscode';
import * as nls from 'vscode-nls';
import * as path from 'path';

import { ICommand } from './command';
import { ExtensionContext } from '../libs/extension-context';
import * as tpl from '../libs/template-processor';

const COMMAND_ID = 'csharp.addApiController';
const localize = nls.config({ messageFormat: nls.MessageFormat.file })();

/**
 * Argument definition for [[AddApiControllerCommand.handle]].
 */
export interface IAddApiControllerCommand {
    /** The path to the directory where to create the class file */
    directory?: vscode.Uri;

    /** The namepace */
    namespace?: string;

    /** The name of the class */
    controllername: string;
}

/**
 * Create a new class file and open it.
 */
export class AddApiControllerCommand implements ICommand<IAddApiControllerCommand, void> {
    public readonly id: string = COMMAND_ID;

    // Constructor
    constructor(readonly context: ExtensionContext) {
    }

    /**
     * Add a class
     *
     * @param command arguments
     */
    public async handle(command: IAddApiControllerCommand): Promise<void> {
        if (!command.controllername || command.controllername === "Controller" || !/^[\p{L}_][\p{L}0-9_]*$/u.test(command.controllername)) {
            throw new Error(localize('warning.noControllerName', "Invalid controller name"));
        }

        if (!command.namespace) {
            command.namespace = await this.context.resolveNamespace(command.directory);
        }

        const destination = command.directory ? vscode.Uri.joinPath(command.directory, `${command.controllername}.cs`) : undefined;
        const data = {
            rootnamespace: command.namespace,
            safeitemname: command.controllername
        };

        new tpl.TemplateProcessor(this.context)
            .process(tpl.TPL_API_CONTROLLER, destination, data)
            .then(document => vscode.window.showTextDocument(document));
    }

    public async convert(context: vscode.Uri): Promise<IAddApiControllerCommand|undefined> {
        let directory = context;
        if (context) {
            const stat = await vscode.workspace.fs.stat(context);
            if (stat.type === vscode.FileType.File) {
                directory = vscode.Uri.file(path.dirname(context.fsPath));
            }
        }
        else if (!context && this.context.projects.length === 1) {
            directory = vscode.Uri.joinPath(this.context.projects[0].rootpath, "Controllers");
        }

        let prompt = localize('prompt.Controllername', "The name of the controller");
        let controllername = await vscode.window.showInputBox({
            placeHolder: 'Controller Name',
            prompt: prompt
        });

        if (controllername === undefined || controllername === null) {
            return undefined;
        }
        if (!controllername.endsWith("Controller")) {
            controllername += "Controller";
        }

        return { directory: directory, controllername: controllername };
    }
}
