/**
 * Common interface to command implementation
 */
export interface ICommand<TCommand,TResponse> {
    /** A unique identifier for the command. */
    readonly id: string;

    /**
     * The command handler
     *
     * @param command Argument of the command
     * @returns The command result
     */
    handle(command: TCommand): Promise<TResponse>;

    /**
     * Convert raw command arguments into `TCommand`
     *
     * @param args Raw command argument
     */
    convert(...args: any[]): Promise<TCommand|undefined>;
}
