import * as path from 'path';
import * as vscode from 'vscode';

import { promises as fs } from 'fs';
import * as ext from './libs/extension-context';

import { ICommand, AddClassCommand, OpenSlnCommand, AddApiControllerCommand } from './commands';

// this method is called when your extension is activated
export async function activate(vscontext: vscode.ExtensionContext) {
    let context = await ext.factory(vscontext);

    let registerCommand = function<TCommand, TResponse>(command: ICommand<TCommand, TResponse>): ICommand<TCommand, TResponse> {
        context.subscriptions.push(
            vscode.commands.registerCommand(
                command.id,
                (...args: any[]) => command.convert(...args)
                                    .then(result => result && command
                                        .handle(result)
                                        .catch(reason => {
                                            vscode.window.showWarningMessage(reason.message);
                                        })
                                    )
                                    .catch(reason => {
                                        console.warn(reason);
                                    }),
                command));
        return command;
    };

    let commands = {
        addClass: registerCommand(new AddClassCommand(context)),
        addApiController: registerCommand(new AddApiControllerCommand(context)),
        openSln: registerCommand(new OpenSlnCommand(context))
    };

    try {
        var buffer = await fs.readFile(path.join(context.extensionPath, 'package.json'));
        let extension = JSON.parse(buffer.toString());
        console.log(`[${extension.name}] v${extension.version} activated`);
    }
    catch (ex) {
        console.error(ex);
    }

    // return the public API
    return {
        addClass: (classname: string, namespace?: string, directory?: vscode.Uri) => commands.addClass.handle({ namespace: namespace, classname: classname, directory: directory }),
        addApiController: (controllername: string, namespace?: string, directory?: vscode.Uri) => commands.addApiController.handle({ namespace: namespace, controllername: controllername, directory: directory }),
        openSln: (sln: vscode.Uri) => commands.openSln.handle({ sln: sln }),
        resolveNamespace: (uri: vscode.Uri) => context.resolveNamespace(uri)
    };
}

// this method is called when your extension is deactivated
export function deactivate() {}
